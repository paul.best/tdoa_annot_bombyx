TDOA Annotation for BOMBYX
==================

`get_track_BOMBYX_7.py` is a graphic utility made for the manual annotation of sperm whale tracks.


Usage
-----

For the moment, the path are hard coded. Thus, the script can be launched by typing 

`python get_track_BOMBYX_7.py`


![GUI example](interace_bombyx.png "GUI example")

The GUI is split into two parts.

The top plot shows the TDOA of the clicks detected. Vertical bars separate the temporal regions that have been concatenated together.

Clicking on a point will then display the metadata of this point above the graph and showed the spectrogram of a 10 seconds window around the selected click. Meanwhile, the sound of this windowed signal will be played.

Below the spectrogram are three buttons. Clicking on one will assign the corresponding label to the selected click.
