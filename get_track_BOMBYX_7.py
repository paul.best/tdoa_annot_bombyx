
import matplotlib.pyplot as plt
import pandas as pd
import scipy.signal as signal
import soundfile as sf
from scipy.io import wavfile
from matplotlib.widgets import Button
import numpy as np
import os
import time
from wavfile import *
from tqdm import tqdm
from audio_v2 import * #Jan code
from pydub import AudioSegment
from pydub.playback import play
import sounddevice as sd
#load df
print('loading df... ', end='')
df = pd.read_pickle('/home/glotinh/Documents/bombyx/dist2/results/clickfiles_maxence/dataframes/BOMBYX7_part/bombyx7_part10')
convert = lambda fn : pd.to_datetime(fn, format="%Y%m%d_%H%M%S_UTC_BOMBYX 7.tdoa", exact=False).strftime("%Y-%m-%d_%H0000_UTC/%Y%m%d_%H%M%S_UTC_BOMBYX 7.wav")

df['folder_ok']="/home/glotinh/Documents/bombyx/dist2/BOMBYX7"

print('done')

#cast columns into floats
df["folder_2"]=pd.to_datetime(df.file, format="%Y%m%d_%H%M%S_UTC_BOMBYX 7.tdoa", exact=False).dt.strftime("%Y-%m-%d_%H0000_UTC/%Y%m%d_%H%M%S_UTC_BOMBYX 7.wav")
df["folder_final"]=df.folder_ok+"/"+df.folder_2
song= WavFile(df.folder_final.iloc[0])
rate=song.sample_rate
fs=song.samples
sos = signal.butter(5, 5000*2/rate, 'hp', output='sos')

df.time = df.time.astype(float)/rate
df.argmax = df.argmax.astype(float)
df.centroid = df.centroid.astype(float)
df.ipi = df.ipi.astype(float)
df.tdoa = df.tdoa.astype(float)
#df.duration = df.duration.astype(float)
df.sal_pip = df.sal_pip.astype(float)
df.energy = df.energy.astype(float)
df["duration"]=900
#sort ordered by filename == by date
df.sort_values('file', inplace=True)

#fileDurations = [df[df.filename==fn].iloc[0].duration + 5 for fn in filenames]
grped = df.groupby('file')

#build array of cumulated durations
# same array but with the length of the df
allfilestart = []
filestarts = [0]
for i, (fn, gp) in enumerate(grped):
    filestarts.append(filestarts[-1] + gp.duration.iloc[0]+5)
    allfilestart.append(np.array(gp.time) + filestarts[i])
    # allfilestart.extend( [filestarts[i]] * len(df[df.filename==fn]) )

filestarts = filestarts[:-1]

df['abs_click_time'] = np.concatenate(allfilestart, 0)

shortfiles = [f.split('/')[-1][:15] for f in df.file.unique()]
#extract top decile
#topdecile = df[ pd.qcut(df.amplitude, 10, labels=False) == 9 ]

# get session sampling rate and create HP filter
#df["path"]=df.folder +'/'+ df.file



fig, ax = plt.subplots(2)
ax[0].scatter(df.abs_click_time, df.tdoa, 1, picker=5)
ax[0].set_xticks(filestarts)
ax[0].set_xticklabels(shortfiles)
plt.xticks(rotation=30)
#ligne bleu
for f in filestarts:
    ax[0].axvline(f, alpha=.5)

winsize = 2.5 # in seconds
class ppp(object):
    def __init__(self):
        self.found=None
    def onclick(self, event):
        #get row
        self.found = df.iloc[np.random.choice(event.ind)]

        ax[0].set_title(self.found.file+'\n Amplitude: '+str(self.found.energy)+' IPI: '+str(self.found.ipi)+' IPI Salience: '+str(self.found.sal_pip)+
            ' \n Spec amax: '+str(self.found.argmax)+' Spec centr:'+str(self.found.centroid))
        ax[0].scatter(self.found.abs_click_time, self.found.tdoa, c='r')

        #get click signal, and filter
        filename = self.found.file[1:] if self.found.file.startswith('../') else self.found.file

        #fs, sig = wavfile.read('../dist2/'+filename, mmap=True)
        #song= WavFile(self.found.folder_final)
        print("read wafile")
        Song=WavFile("/home/glotinh/Documents/bombyx/dist2/BOMBYX7/"+convert(self.found.file))
        print("on recup le sample rate")
        rate=Song.sample_rate
        #print("on recup les samples")
        #fs=Song.samples
        print("on recup la pos du clic")
        clickidx = int(self.found.time * rate)
        print("on selectionne le clic")
        sig = Song[clickidx-int(winsize*.5*rate):clickidx+int(winsize*.5*rate),0].copy() # select the click !
        print("le clic est selectionné")
        #filtered = signal.sosfilt(sos, sig)
        ax[1].clear()
        #ax[1].plot(filtered)
        ax[1].specgram(sig,Fs=rate)
        #lire le sons
        print("on lis le sons")
        sd.play(sig,rate)
        print("le sons est lu")
        fig.canvas.draw()

    def yes(self, event):
        print(self.found.name)
        df.at[self.found.name,'labels']='cachcach'

    def no(self, event):

        df.at[self.found.name,'labels']='botbot'

    def glob(self, event):

        df.at[self.found.name,'labels']='globi'

df["labels"]=None

callback = ppp()


axyes = plt.axes([0.7, 0.05, 0.1, 0.075])
axno = plt.axes([0.81, 0.05, 0.1, 0.075])
axglob = plt.axes([0.5, 0.05, 0.1, 0.075])

bnext = Button(axyes, 'is cachcach')
bnext.on_clicked(callback.yes)

bprev = Button(axno, 'is boat')
bprev.on_clicked(callback.no)

bglob = Button(axglob, 'is globi')
bglob.on_clicked(callback.glob)


cid = fig.canvas.mpl_connect('pick_event', callback.onclick)
plt.show()
df.to_pickle('BOMBYX_2017_BOMBYX7_part10_tes.pkl')

# other mask
mask = ((df.centroid>11e3)&(df.centroid<17e3)
       &(df.ipi>2e-3)&(df.ipi<10e-3)
       &(df.tdoa>-1.2e-3)&(df.tdoa<1.2e-3)
       &(df.argmax<20e3))
